# F23_WD2_ColorGame
team members:
Samin Ahmed 2043024
Adam Winter 2242433

description:
the project is a simple colour guessing game with adjustable settings and cheats.
It also has a sortable stats table that can be saved to and loaded as well as cleared.

running
To run it enter a valid name between 4-12 characters
then choose game settings (table size, guessing colour and difficulty)
then click the start button which will remake the game to avoid cheating and start the timer
when you are done guessing press submit and a sound will play depending on your score
your score can be seen in the sortable stats table on the left of the screen