/* eslint-disable no-undef */
/* eslint-disable strict */
// eslint-disable-next-line lines-around-directive
'use strict';

/**
 * @file A JavaScript file for testing functions that will go into utilities
 * @author Adam W
 * @version 1.0
 * @date 2023-10-23
*/
// global variables
let redCounter;
let greenCounter;
let blueCounter;
let page;
/**
 * @description
 * getHighestColour takes an rgb input and returns a string
 * that represents the highest colour of that rgb
 * @param RGBValues a string representing an rgb colour (ex: rgb(255,170,60))
 * @returns the most predominant colour in the inputted rgb string (ex: red)
 */
function getHighestColour(RGBValues) {
  const regex = /rgb\((\d+), (\d+), (\d+)\)/;
  const RGBcolours = regex.exec(RGBValues).slice(1).map((a) => parseInt(a, 10));

  let highestColourValue = 0;
  RGBcolours.forEach((elem) => {
    if (elem > highestColourValue) {
      highestColourValue = elem;
    }
  });
  const colourIndex = RGBcolours.indexOf(highestColourValue);
  let highestColour;

  switch (colourIndex) {
    case 0:
      highestColour = 'red';
      break;
    case 1:
      highestColour = 'green';
      break;
    case 2:
      highestColour = 'blue';
      break;
    default:
      break;
  }
  return highestColour;
}

/**
 * @description
 * Determines a range for the colours based on game difficulty
 * the more difficult the game the closer each colour is to the others
 * uses @function randomBetween to generate numbers in the range
 * @returns an rgb string to be used as a background colour on elements
 * @see randomBetween
*/
function generateColor() {
  function randomBetween(minValue, maxValue) {
    if (maxValue > 255) {
      // eslint-disable-next-line no-param-reassign
      maxValue = 255;
    }
    if (minValue < 0) {
      // eslint-disable-next-line no-param-reassign
      minValue = 0;
    }
    const ran = Math.floor(Math.random() * (maxValue - minValue) + minValue);
    return ran;
  }

  let range;

  switch (page.complexitySelect.value) {
    case 'easy':
      range = 255;
      break;
    case 'medium':
      range = 80;
      break;
    case 'difficult':
      range = 50;
      document.querySelector('#game_table').style.color = 'crimson';
      break;
    case 'brutal':
      range = 10;
      document.querySelector('#game_table').style.color = 'crimson';
      break;
    default:
      break;
  }

  const red = randomBetween(0, 255);
  const blue = randomBetween(red - range, red + range);
  const green = randomBetween(red - range, red + range);

  const rgb = `rgb(${red}, ${green}, ${blue})`;

  return rgb;
}
/**
 * @description
 *changes the game table's cells to have an orange border when they're clicked on
 * @param {*} event
 */
function updateCellBorder(event) {
  page.clicks += 1;
  const ev = event;

  if (ev.target.nodeName === 'TD' && gameStart === true) {
    if (ev.target.style.border !== '') {
      ev.target.style.border = '';
    } else {
      ev.target.style.border = '0.2em solid orange';
    }
  } else if (ev.target.parentNode.nodeName === 'TD' && gameStart === true) {
    if (ev.target.parentNode.style.border !== '') {
      ev.target.parentNode.style.border = '';
    } else {
      ev.target.parentNode.style.border = '0.2em solid orange';
    }
  }
}

/**
 * @description
 * dynamically generates the game table and places it into the table_sec section of the html
 * uses @function generateColor to generate a random rgb value
 * and set it as the bakcground for each cell
 * also sets text on the generated cells
 * representing the most predomiant colour in its background rgb
 * adds an event to the table that calls
 * @function updateCellBorder whenever it or a child node is clicked
 * @param cells a number representing the size of the game matrix
 * @see generateColor
 * @see updateCellBorder
 */
function generateTable(cells) {
  if (document.querySelector('#game_table')) {
    const elem = document.querySelector('#game_table');
    elem.parentNode.removeChild(elem);
  }
  // create initial table and its body with new lines for readability in text area
  const table = createMyElement(document.querySelector('#table_sec'), 'table', 'game_table');
  table.innerHTML += '\n<tbody>\n</tbody>\n';

  // reset colour counters when a new table is made
  redCounter = 0;
  greenCounter = 0;
  blueCounter = 0;
  // generate a cellxcell matrix table
  for (let i = 0; i < cells; i += 1) {
    table.children[0].innerHTML += '  <tr>\n  ';
    for (let j = 0; j < cells; j += 1) {
      let text = 'err';
      const rgb = generateColor();
      // rgb = "rgb(170,197,60)"

      // find the highest colour and update the text in the td +
      // a counter for the amount of that colour
      switch (getHighestColour(rgb)) {
        case 'red':
          text = '0';
          redCounter += 1;
          break;
        case 'green':
          text = '1';
          greenCounter += 1;
          break;
        case 'blue':
          text = '2';
          blueCounter += 1;
          break;
        default:
          break;
      }

      table.children[0].children[i].innerHTML += `  <td> ${text} `;
      table.children[0].children[i].children[j].style.backgroundColor = rgb;
    }
    table.children[0].children[i].innerHTML += '\n  ';
    table.children[0].innerHTML += '\n';
    table.addEventListener('click', updateCellBorder, false);
  }
}

/**
 * @description
 * updates the page objects gameColour variable to have:
 * rgb : an rgb string representing the chosen game colour (ex:255,0,0)
 * colour : a string to show the chosen game colour (ex: red)
 */
function findGameColour() {
  if (page.radioRed.checked) { page.gameColour = 'red'; }
  if (page.radioGreen.checked) { page.gameColour = 'green'; }
  if (page.radioBlue.checked) { page.gameColour = 'blue'; }
}

/**
 * @description
 * runs whenever the bumit button is pressed.
 * It records all the tiles that the player has selected
 * and calcuates their score based on the chosen colour to guess
 * for each  wrong answer the player loses 20%
 */
function submitAnswer() {
  const penalty = 0.2;
  let amountRedSelected = 0;
  let amountGreenSelected = 0;
  let amountBlueSelected = 0;
  let wrongGuesses;
  page.accuracy = 0;
  let backgroundRGB;
  const TDs = document.querySelectorAll('#game_table td');

  // checks how much of each predominant colour was selected
  TDs.forEach((td) => {
    if (td.style.border !== '') {
      backgroundRGB = td.style.backgroundColor;
      const highest = getHighestColour(backgroundRGB);

      switch (highest) {
        case 'red':
          amountRedSelected += 1;
          break;
        case 'green':
          amountGreenSelected += 1;
          break;
        case 'blue':
          amountBlueSelected += 1;
          break;
        default:
          break;
      }
    }
  });
  switch (page.gameColour) {
    case 'red':
      page.accuracy = amountRedSelected / redCounter;
      wrongGuesses = amountGreenSelected + amountBlueSelected;
      page.accuracy = (page.accuracy - penalty * wrongGuesses) * 100;
      break;
    case 'green':
      page.accuracy = amountGreenSelected / greenCounter;
      wrongGuesses = amountRedSelected + amountBlueSelected;
      page.accuracy = (page.accuracy - penalty * wrongGuesses) * 100;
      break;
    case 'blue':
      page.accuracy = amountBlueSelected / blueCounter;
      wrongGuesses = amountGreenSelected + amountRedSelected;
      page.accuracy = (page.accuracy - penalty * wrongGuesses) * 100;
      break;
    default:
      break;
  }
  if (page.accuracy < 0) { page.accuracy = 0; }

  page.accuracy = Math.round(page.accuracy * 100) / 100;
  if (page.accuracy === 100) { page.successSound.play(); }
  if (page.accuracy <= 50) { page.failureSound.play(); }

  submitGame();
}

/**
 * @description
 *changes the text in the game prompt when the player chooses a new guessing colour
 */
function updateText() {
  const promptColour = document.querySelector('#color_span');
  promptColour.innerText = page.gameColour;
  promptColour.style.backgroundColor = page.gameColour;
  const colourCounter = document.querySelector('#count_span');
  switch (page.gameColour) {
    case 'red':
      colourCounter.textContent = `you need to guess ${redCounter} red tile(s)`;
      break;
    case 'green':
      colourCounter.textContent = `you need to guess ${greenCounter} green tile(s)`;
      break;
    case 'blue':
      colourCounter.textContent = `you need to guess ${blueCounter} blue tile(s)`;
      break;
    default:
      break;
  }
}

/**
 * @description
 * Sets the start button's background to be the input colour
 * @param colour the colour to set the start button's background to
 */
function updateStartButtonColour(colour) {
  page.startBtn.style.backgroundColor = colour;
}

function updateOtherButtonColours(colour) {
  if (page.validName) {
    document.getElementById('submit').style.backgroundColor = colour;
  }
  document.getElementById('save_button').style.backgroundColor = colour;
  document.getElementById('load_button').style.backgroundColor = colour;
  document.getElementById('clear_button').style.backgroundColor = colour;
}
/**
 * @description
 * stops showing the background colour's rgb values on the game table's cells
 */

function disableCheats() {
  const spans = document.querySelectorAll('.cheats');
  spans.forEach((elem) => {
    elem.parentNode.removeChild(elem);
  });
}

/**
 * @description
 * shows the background colour's RGB value on the game table's cells
 */
function enableCheats() {
  disableCheats(); // make sure rgbs arent already shown
  const TDs = document.querySelectorAll('#game_table td');
  let rgb;
  TDs.forEach((elem) => {
    rgb = elem.style.backgroundColor;
    const td = elem;
    td.innerHTML += `<span class = "cheats"> ${rgb}`;
  });
}

/**
 * @description
 * makes sure the game table has atleast one of each colour, otherwise regenerates it
 */
function validateGameTable() {
  while (redCounter === 0 || blueCounter === 0 || greenCounter === 0) {
    generateTable(page.cells.value);
  }
}

/**
 * @description
 * verifies the player name to be alphanumeric between 3-12 characters.
 * if its valid the input field has a green background
 * if its invalid shows an error message and a red background
 */
function validateName() {
  if (!gameStart) {
    const regex = /\w{4,12}/;
    const playerName = page.playerNameinput.value;

    if (!regex.test(playerName)) {
      page.playerNameinput.style.backgroundColor = 'lightcoral';
      page.playerNameinput.placeholder = 'alphanumeric 4-12 chars';
      updateStartButtonColour('lightgrey');
      document.getElementById('start_button').disabled = true;
      submitButton.disabled = true;
      submitButton.style.backgroundColor = 'lightgrey';
      submitButton.innerText = 'NOT READY';
      page.validName = false;
    } else {
      page.playerNameinput.style.backgroundColor = 'lightgreen';
      document.getElementById('start_button').disabled = false;
      updateStartButtonColour(page.gameColour);
      updateOtherButtonColours(page.gameColour);
      submitButton.style.backgroundColor = page.gameColour;
      submitButton.innerText = 'SUBMIT YOUR  GUESSES! (START FIRST)';
      page.validName = true;
    }
  }
}

/**
 * handles all the initializing and validation of the game
 */
function initialize() {
  page.clicks = 0;
  if (page.cells.value > 6) {
    page.cells.value = 6;
  }
  if (page.cells.value < 3) {
    page.cells.value = 3;
  }
  const cellCount = page.cells.value;
  generateTable(cellCount);
  findGameColour();
  updateText();
  validateGameTable();
  validateName();
  updateOtherButtonColours(page.gameColour);
  if (document.getElementById('cheats').checked) {
    enableCheats();
  }
}

/**
   * @description
   * adds events to all the objects on the page
   */
// eslint-disable-next-line no-unused-vars
function events() {
  page.playerNameinput.addEventListener('blur', validateName);
  page.radios.forEach((element) => {
    element.addEventListener('click', findGameColour);
    element.addEventListener('click', updateText);
    element.addEventListener('click', () => {
      if (page.validName) {
        updateStartButtonColour(page.gameColour);
      }
    });
    element.addEventListener('click', () => { updateOtherButtonColours(page.gameColour); });
  });

  page.startBtn.addEventListener('click', initialize);
  page.cells.addEventListener('change', initialize);
  page.complexitySelect.addEventListener('change', initialize);
  page.submitBtn.addEventListener('click', submitAnswer);
  page.submitBtn.addEventListener('click', enableCheats);

  document.getElementById('cheats').addEventListener('change', (Event) => {
    if (Event.target.checked) {
      enableCheats();
    } else {
      disableCheats();
    }
  });
}

document.addEventListener('DOMContentLoaded', () => {
  redCounter = 0;
  blueCounter = 0;
  greenCounter = 0;

  page = {
    playerNameinput: document.getElementById('player_name_input'),
    gameColour: 'null',
    cells: document.querySelector('#table_size_input'),
    radios: document.getElementsByName('color'),
    radioRed: document.querySelector('#red'),
    radioGreen: document.querySelector('#green'),
    radioBlue: document.querySelector('#blue'),
    complexitySelect: document.querySelector('#difficulty'),
    submitBtn: document.querySelector('#submit'),
    startBtn: document.querySelector('#start_button'),
    accuracy: 0,
    clicks: 0,
    validName: false,
    successSound: new Sound('../AUDIO/success.mp3'),
    failureSound: new Sound('../AUDIO/failure.mp3'),
  };
  events();
  initialize();
});
