/* eslint-disable no-undef */
/* eslint-disable no-restricted-syntax */
/* eslint-disable strict */

'use strict';

/**
 * @file A JavaScript file for manipulating (inputting and sorting) a stats table and array.
 * @author Sam A
 * @version 1.0
 * @date 2023-10-23
 */

// eslint-disable-next-line prefer-const
let statsArray = [];
const tableBody = document.getElementById('stats_table_body');
const startButton = document.getElementById('start_button');
const submitButton = document.getElementById('submit');
const timerP = document.getElementById('timerText');

let secondsTimer;
let seconds = 0;
let timerText = '0:00';
let gameStart = false;
/**
* This function clears the stats table of its contents using innerHTML.
*/
function clearStatsTable() {
  tableBody.innerHTML = ' ';
}

/**
 * Inserts stats rows into the table using the array (array of stats objects) as input
 * @param {Object} statsArray - Array of stat object
 */

function addRows(inputArray) {
  clearStatsTable();
  if (inputArray.length !== 0) {
  // Loop through the array of input objects
    inputArray.forEach((inputObject) => {
      // Create a new row for each object
      const newRow = document.createElement('tr');
      // Iterate through the keys in the input object and create table cells
      for (const key in inputObject) {
        if (key !== 'durationS') {
          const cell = document.createElement('td');
          cell.textContent = inputObject[key];
          newRow.appendChild(cell);
        }
      }

      // Append the new row to the table body
      tableBody.appendChild(newRow);
    });
  }
}

/**
     * This function disables all the settings inputs once the game starts.
     */
function disableInputs() {
  page.playerNameinput.disabled = true;
  page.complexitySelect.disabled = true;
  page.cells.disabled = true;
  page.radios.disabled = true;
  page.complexitySelect.disabled = true;
  page.radioRed.disabled = true;
  page.radioGreen.disabled = true;
  page.radioBlue.disabled = true;
}

/**
     * This function uses a "seconds" to compute a string that has the time in it (MM:SS)
     * It then updates inner text to display the timer on the page.
     */
function secondsToTimer() {
  const minutes = parseInt(seconds / 60, 10);
  let secondsLeft = seconds - (minutes * 60);
  if (secondsLeft < 10) {
    secondsLeft = `0${secondsLeft}`;
  }
  timerText = `${minutes}:${secondsLeft}`;
  timerP.innerText = timerText;
}

/**
     * This function increments seconds by 1 every time it's called.
     * Once added to, it updates the text using secondsToTimer()
     */
function addSecond() {
  seconds += 1;
  secondsToTimer();
}

/**
         * This function handles the starting of the game once the start button is clicked.
         * It does the following (IF GAME START IS FALSE)
         *      -Sets gameStart to true.
         *      -Enables the submit button.
         *      -Sets seconds and clicks back to 0.
         *      -Sets the website timer display back to 0:00.
         *      -Changes the submit button text
         *      -Starts the timer
         *      @see disableInputs() : Disables inputs
         */
function startGame() {
  if (gameStart === false) {
    gameStart = true;
    submitButton.disabled = false;
    startButton.disabled = true;
    seconds = 0;
    timerP.innerText = '0:00';
    submitButton.innerText = 'SUBMIT YOUR GUESSES!';
    secondsTimer = setInterval(addSecond, 1000);
    disableInputs();
    page.clicks = 0;
  }
}

startButton.addEventListener('click', startGame);

/**
     * This function clears all the headings in the stats table
     * by resetting them to their default states.
     */
function clearHeadings() {
  playerHeading.innerText = 'Player';
  durationHeading.innerText = 'Duration(s)';
  clicksHeading.innerText = 'Clicks';
  levelHeading.innerText = 'Level';
  successHeading.innerText = 'Success(%)';
  sizeHeading.innerText = 'Size';
}

/**
     * This function enables all the settings inputs once the game starts.
     */
function enableInputs() {
  page.playerNameinput.disabled = false;
  page.complexitySelect.disabled = false;
  page.cells.disabled = false;
  page.radios.disabled = false;
  page.complexitySelect.disabled = false;
  page.radioRed.disabled = false;
  page.radioGreen.disabled = false;
  page.radioBlue.disabled = false;
}

/**
     * This function handles the submission of the game once the submit button is clicked.
     * It does the following
     *      - Creates a stat object using the game results and settings.
     *      - Pushes the stat object into the stats array
     *      - Clears the interval timer
     *      - Sets seconds back to 0 and the displayed timer text to "0:00"
     *      - Sets submit button text back
     *      - Disables the submit button
     *      - Redraws the stats table using newly added to statsArray
     *      @see enableInputs() : Enables inputs
     *      @see clearHeadings() : clearsHeadings
     */
// eslint-disable-next-line no-unused-vars
function submitGame() {
  const stat = {
    player: page.playerNameinput.value,
    duration: timerText,
    durationS: seconds,
    clicks: page.clicks,
    level: page.complexitySelect.value,
    success: page.accuracy,
    size: page.cells.value,
  };

  statsArray.push(stat);
  clearHeadings();
  clearInterval(secondsTimer);
  seconds = 0;
  timerP.innerText = '0:00';
  enableInputs();
  gameStart = false;
  submitButton.innerText = 'SUBMIT YOUR GUESSES! (START FIRST)';
  submitButton.disabled = true;
  startButton.disabled = false;

  addRows(statsArray);
}
